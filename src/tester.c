#include "tester.h"
#include <stdio.h>
#include <stdlib.h>

#include "mem_debug.h"

void test1(void* heap){
    const char* description = ;
    printf("First test\n");
    void* block =_malloc(75);
    if(block==NULL){{ fprintf(stderr,"First test failed");}}
    debug_heap(stdout, heap);
    _free(block);
    printf("First test finished \n");
}

void test2(void* heap){
    printf("Second test\n");
    void* block_1 =_malloc(70);
    void* block_2 =_malloc(30);
    if(block_1==NULL ||block_2==NUL){ fprintf(stderr,"Second test failed");}
    debug_heap(stdout, heap);
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2);
    printf("Second test finished\n");
}

void test3(void* heap){
    printf("Third test\n");
    void* block_1 =_malloc(70);
    void* block_2 =_malloc(30);
    void* block_3 =_malloc(5);
    if(block_1==NULL ||block_2==NULL||block_3==NULL){ fprintf(stderr,"Third test failed");}
    debug_heap(stdout, heap);
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);
    _free(block_3);
    printf("Third test finished\n");
}

void test4(void* heap){
    printf("Fourth test\n");
    void* block = _malloc(18000);
    if(block==NULL){ fprintf(stderr,"Fourth test failed");}
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    printf("Fourth test finished\n");
}

void test5(void* heap){
    printf("Fifth test\n");
    void* block_1=_malloc(1000000);
    void* block_2=_malloc(50);
    debug_heap(stdout, heap);
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2)
    debug_heap(stdout, heap);
    printf("Fifth test finished\n");
}
void run_tests(void* heap){
    test1(heap);
    test2(heap);
    test3(heap);
    test4(heap);
    test5(heap);
}