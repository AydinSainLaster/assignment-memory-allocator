//
// Created by Sainl on 06.02.2023.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTER_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTER_H
void test1(void* heap);
void test2(void* heap);
void test3(void* heap);
void test4(void* heap);
void test5(void* heap);
void run_test(void* heap);
#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTER_H
